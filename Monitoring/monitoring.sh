#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;
BASEDIR=$(pwd);
if [ ! $LOG_DIR ]; then
	LOG_DIR=$BASEDIR
fi;

./monitor_processes.sh 2>&1 >> $LOG_DIR/monitor_processes.log &
