#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;
BASEDIR=$(pwd);
if [ ! $LOG_DIR ]; then
	LOG_DIR=$BASEDIR
fi;

while true; do
	ps -e | grep java
	ps -e | grep ssh
	# sleep 10 min
	sleep 600;
done
