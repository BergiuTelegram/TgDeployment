#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

source base.sh

if [ -z $1 ]; then
	# deploy all
	deploy_all $@;
else
	#deploy $1
	deploy_one $@;
fi;
exit $?;
