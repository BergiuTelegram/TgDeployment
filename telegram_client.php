<?php
require_once("vendor/autoload.php");
require_once("config.php");
if(empty($telegram_bot_token)){
	echo "You fist need to save your Telegram Bot Token in settings.php. Have a look at README.md for install instructions.";
	exit(1);
}
if(empty($telegram_allowed_user_ids)){
	echo "You fist need to save your Telegram Chat Ids in settings.php. Have a look at README.md for install instructions.";
	exit(1);
}
if(empty($telegram_bot_username)){
	echo "You fist need to save your Telegram Bot Username in settings.php. Have a look at README.md for install instructions.";
	exit(1);
}

$tg = new telegramBot($telegram_bot_token);
print_r( $tg->getMe());
echo "Allowed user ids:\n";
foreach($telegram_allowed_user_ids as $id){
	echo "    $id\n";
}

$events = array(
	array("/^\/ping$/","PONG!"),
	array("/^\/ping\_google$/",function(){return shell_exec("ping -c 2 google.de");}),
	/* array("/^Hello Bot$/",function($data){return "Hello ".$data['message']['from']['first_name'];}), */
	array("/^\/ps (.+)$/",function($data, $groups){return shell_exec("ps -e | grep -i $groups[1]");}),
	array("/^\/deploy$/",function(){shell_exec("./deploy.sh");}),
	array("/^\/deploy (.+)$/",function($data, $groups){shell_exec("./deploy.sh $groups[1]");}),
	array("/^\/run$/",function(){shell_exec("./run.sh");}),
	array("/^\/run (.+)$/",function($data, $groups){shell_exec("./run.sh $groups[1]");}),
	array("/^\/shell (.+)$/",function($data, $groups){return shell_exec($groups[1]);}),
	array("/^\/list_projects$/",function(){return shell_exec("cat deploy_list.txt");})
);

$offset=0;
do
{
	// Get updates the bot has received
	// Offset to confirm previous updates
	$updates = $tg->pollUpdates($offset);
	if ($updates['ok'] && count($updates['result']) > 0) {
		foreach($updates['result'] as $data) {
			if(!array_key_exists('message', $data)){
				# no message
				continue;
			}
			if(!array_key_exists('text', $data['message'])){
				# no text
				continue;
			}
			if(!array_key_exists('from', $data['message'])){
				# no user (from channel)
				continue;
			}
			$message = $data['message']['text'];
			$chat_id = $data['message']['chat']['id'];
			$user_id = $data['message']['from']['id'];
			if(!in_array($user_id, $telegram_allowed_user_ids)){
				# chat id now allowed
				echo "user not allowed: $user_id\n";
				continue;
			}
			$event = null;
			foreach ($events as $arr){
				$pattern = $arr[0];
				$message = str_replace($telegram_bot_username, "", $message);
				if(preg_match($pattern, $message, $matches)){
					$event = $arr[1];
					break;
				}
			}
			if(empty($event)){
				# no command found
				continue;
			}
			if(gettype($event)=='object'){
				$response = $event($data,$matches);
			} else {
				$response = $event;
			}
			if(!empty($response)){
				$tg->sendMessage($chat_id, $response);
			}
		}
		$offset = $updates['result'][count($updates['result']) - 1]['update_id'] + 1;
	}
}
while(true);


?>
