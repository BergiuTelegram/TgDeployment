<?php
/**
 * Send a message to telegram chat. the message could use the telegram HTML syntax.
 * Every Argument is send in a new line of the message, for example:
 *   php send_telegram_msg "Hallo Welt" "Zweite Zeile" "Dritte Zeile"
 **/
require_once("config.php");
require_once("vendor/autoload.php");
if(empty($telegram_bot_token)){
	echo "You fist need to save your Telegram Bot Token in settings.php. Have a look at README.md for install instructions.";
	exit(1);
}
if(empty($telegram_chat_ids)){
	echo "You fist need to save your Telegram Chat Ids in settings.php. Have a look at README.md for install instructions.";
	exit(1);
}
if(empty($argv[1])){
	echo "You at least need to specify a message to send.";
	exit(1);
}
$message = null;
if($argv[1]=="--send-file-contents"){
	if(empty($argv[2])){
		echo "you must specify a file if you user the --file param";
		exit(1);
	}
	$filename = $argv[2];
	$handle = fopen($filename, "r");
	if($handle){
		while (($line = fgets($handle)) !== false) {
			//process the readline
			if($message===null){
				$message="$line";
			} else {
				$message.="$line";
			}
		}
	} else {
		echo "can't open file";
		exit(1);
	}
} else {
	foreach($argv as $param){
		if($message===null){
			$message="";
		} else {
			$message.="\n$param";
		}
	}
}
$tg = new telegramBot($telegram_bot_token);

foreach($telegram_chat_ids as $telegram_chat_id){
	$tg->sendMessage($telegram_chat_id, $message, "HTML");
}
?>
