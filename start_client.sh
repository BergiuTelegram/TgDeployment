#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;
if [ ! $LOG_DIR  ]; then
	LOG_DIR=$BASEDIR
fi;

php telegram_client.php &

PID=$!;
echo $PID>$BASEDIR/Instances/automatic_deployment_client_instance.txt 2>> $LOG_DIR/automatic_deployment_client.run.log;
echo "$PID";
