#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

source base.sh

if [ -z $1 ]; then
	# run all
	run_all $@;
else
	# run $1
	run_one $@;
fi;
exit $?;
