#!/bin/bash
if [ $0 ]; then
	BASEDIR=$(dirname "$0");
else
	BASEDIR=$(pwd);
fi;
cd $BASEDIR;
BASEDIR=$(pwd);
if [ ! $LOG_DIR ]; then
	LOG_DIR=$BASEDIR
fi;

function deploy {
	cd $BASEDIR;
	folder=$1;
	add_to_log_unencoded "<b>------------------------------"
	add_to_log "Deploy: $folder";
	add_to_log "($(date))";
	add_to_log_unencoded "------------------------------</b>"
	add_to_log_unencoded "<code>"
	folder_tmp="${folder}_tmp"
	# check if deployment is possible
	if [ -d ${folder_tmp} ]; then
		add_to_log "there is already a deployment started";
		add_to_log_unencoded "</code>"
		return 1;
	fi;
	# make deployment folder
	add_to_log "Copy all to new version folder";
	add_to_log `cp -R ${folder} ${folder_tmp} 2>&1`;
	cd ${folder_tmp};
	# checkout the rigth branch
	remote="origin";
	branch="master";
	add_to_log "a";
	add_to_log `git checkout ${branch} 2>&1`
	ret=$?;
	if [ $ret -ne 0 ]; then
		# error
		cd $BASEDIR;
		add_to_log `rm -Rf ${folder_tmp} 2>&1`;
		add_to_log_unencoded "</code>"
		handle_error $ret "git checkout failed on ${folder}";
		return 1;
	fi;
	# check for new updates
	if [ "`git fetch 2>&1`" = "" ]; then
		# no updates
		cd $BASEDIR;
		add_to_log `rm -Rf ${folder_tmp} 2>&1`;
		add_to_log_unencoded "</code>"
		add_to_log_unencoded "<b>------------------------------";
		add_to_log "Already up-to-date";
		add_to_log_unencoded "------------------------------</b>";
		add_to_log "";
		return 1;
	else
		# updates
		# pull
		add_to_log "Updating";
		add_to_log `git pull 2>&1`;
		ret=$?;
		if [ $ret -ne 0 ]; then
			# error
			cd $BASEDIR;
			add_to_log `rm -Rf ${folder_tmp} 2>&1`;
			add_to_log_unencoded "</code>"
			handle_error $ret "git pull failed on ${folder}";
			return 1;
		fi;
		# build
		add_to_log "Compiling";
		add_to_log `./build 2>&1`;
		ret=$?;
		if [ $ret -ne 0 ]; then
			# error
			cd $BASEDIR;
			add_to_log `rm -Rf ${folder_tmp} 2>&1`;
			add_to_log_unencoded "</code>"
			handle_error $ret "build failed on ${folder}";
			return 1;
		fi;
		# if all works
		cd $BASEDIR;
		# copy back
		add_to_log "Override old version with new version";
		add_to_log `rm -Rf ${folder} 2>&1 && mv "${folder_tmp}" ${folder} 2>&1`;
		ret=$?;
		if [ $ret -ne 0 ]; then
			# error
			cd $BASEDIR;
			add_to_log_unencoded "</code>"
			handle_error $ret "moving back failed on ${folder}";
			return 1;
		fi;
		add_to_log `cd ${BASEDIR}`;
		add_to_log `git add . && git commit -m "updated submodule ${folder}"`;
		add_to_log_unencoded "</code>";
		run $folder;
	fi;
	return 0;
}

function run {
	folder=$1;
	add_to_log_unencoded "<b>------------------------------";
	add_to_log "Run: ${folder}";
	add_to_log "($(date))";
	add_to_log_unencoded "------------------------------</b>";
	add_to_log_unencoded "<code>"
	# kill old instance only if its java
	add_to_log "Killing old instance";
	OLD_PID=`cat Instances/${folder}_instance.txt`;
	if [ -e /proc/${OLD_PID}/status ]; then
		add_to_log `kill $OLD_PID 2>&1`;
		add_to_log `kill -9 $OLD_PID 2>&1`;
	else
		# do not kill
		add_to_log "No old instance found";
	fi;
	# run
	add_to_log "Running new version";
	PID=`$BASEDIR/${folder}/run`
	add_to_log `echo $PID>$BASEDIR/Instances/${folder}_instance.txt 2>&1`;
	add_to_log "New version PID=$PID";
	add_to_log_unencoded "</code>"
	add_to_log_unencoded "<b>------------------------------";
	add_to_log "Successfully Finished";
	add_to_log_unencoded "------------------------------</b>";
	add_to_log "";
	return 0;
}

function deploy_one {
	deploy $1;
	write_log;
}

function deploy_all {
	while read in; do
		deploy $in;
		write_log;
	done < deploy_list.txt
}

function run_one {
	run $1;
	write_log;
}

function run_all {
	while read in; do
		run $in;
		write_log;
	done < deploy_list.txt
}

# Writes error messages into an error file and over telegram
# $1 is the exit code
# $2-end is the error message
function handle_error {
	code=$1
	error="";
	i=0;
	for param in $@; do
		if [ $i -ne 0 ]; then
			if [ $i -eq 1 ]; then
				error="$param"
			else
				error+=" $param"
			fi
		fi
		i=$(($i+1));
	done
	add_to_log_unencoded "<b>------------------------------";
	add_to_log "Code: $code";
	add_to_log "Error: $error";
	add_to_log_unencoded "------------------------------</b>";
}

# echo and adds messages to tmp.log
function add_to_log {
	old_ret=$?;
	if [ -n "$1" ]; then
		echo $@;
		echo $@ | sed s/\>/\\\&gt\;/g | sed s/\</\\\&lt\;/g >> $LOG_DIR/tmp.log;
	fi
	return $old_ret;
}

function add_to_log_unencoded {
	old_ret=$?;
	if [ -n "$1" ]; then
		echo $@;
		echo $@ >> $LOG_DIR/tmp.log;
	fi
	return $old_ret;
}

function clear_log {
	rm $LOG_DIR/tmp.log;
	echo "------------------------------" > $LOG_DIR/automatic_deployment.base.log
}

function write_log {
	cat $LOG_DIR/tmp.log >> $LOG_DIR/automatic_deployment.base.log;
	php send_telegram_msg.php --send-file-contents $LOG_DIR/tmp.log;
	clear_log;
}
