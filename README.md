# Automatic Deployment

This Project should build and run other Projects automatically after git commits.

## Features
- You can send notifications after deployment over a Telegram Bot
- Takes care about killing old processes
- You can controll the deployment over Telegram
	- these commands are supportet
		- `/ping` - Pong
		- `/ping_google` - Sends a ping to google
		- `/ps {filter}` - filters ps -e | grep -i $filter
		- `/deploy` - Deploys all
		- `/run` - Restarts all
		- `/deploy {foldername}` - Deploys one project
		- `/run {foldername}` - Restarts one project
		- `/shell {shellcommand}` - Executes shell commands
		- `/list_projects` - Lists all available projects

## Requirements
- git
- PHP and Composer
	- `sudo apt-get install php7.0 php-pear php7.0-mysql php7.0-curl`
	- `sudo apt-get install composer`

## Installation
- run `./install.sh`
- create a telegram bot and place your telegram bot token in `config.php`
- write some messages to your bot and get your chat id from https://api.telegram.org/bot*\<BOT-TOKEN\>*/getUpdates
	- Group-Chats have negative and Normal-Chats and Channels positive numbers 
- add all your chat ids to `config.php`
- also add the user ids, that should be allowed to execute commands on your bot to the telegram\_allowed\_user\_ids array
	- *this should only be your own id*
	- if you allow other users to controll your bot too, your should remove the `"/^\/shell (.+)$/"` line in telegram\_client.php

## Remove Projects from Automatic Deployment
- just remove the foldername from `deploy_list.txt`
- after that you could also remove the folder

## Deploy Requirements
The Programms that should be run must have the following scripts/executables:
- build
	- should build the application
- run
	- should run the application and **must** only echo the process id
	- without the process id there is no way to kill this process later
- make sure these scritps/executables are executable

## Add new Projects to Automatic Deployment
- clone the repos right into this directory as submodule
	- `git submodule add https://github.com/<username>/<projectname> <foldername>`
- add the name of the repo folder into `deploy_list.txt`
- make sure the repo comply with the requirements

## Run
- On System start you should remove all old instances
	`./remove_old_instances.sh`
- Run (restart) all Applications
	`./run.sh`
- Run (restart) only one Application
	`./run.sh foldername`
- Update all Applications and Run only if there are updates
	`./deploy.sh`
- Update only one Application and Run only if there are updates
	`./deploy.sh foldername`
