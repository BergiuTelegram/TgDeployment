#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

source base.sh

handle_error $@
exit $?;
